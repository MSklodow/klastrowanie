# download PFAM domain definitions
download_pfam_pdb_map() {
    curl -O  https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/pdbmap.gz
    gzip -d pdbmap.gz
    mv pdbmap pfam_pdb_map.txt
}

# download chain sequences from PDB
download_sequences() {
    curl -O https://files.wwpdb.org/pub/pdb/derived_data/pdb_seqres.txt.gz
    gzip -d pdb_seqres.txt.gz
}


# filter the sequences
filter_sequences() {
    $HOME/src.git/bioshell4/target/release/examples/filter_fasta pdb_seqres.txt -u --description-contains "mol:protein" > unique_protein.fasta
}

#download_pfam_pdb_map
#download_sequences
filter_sequences