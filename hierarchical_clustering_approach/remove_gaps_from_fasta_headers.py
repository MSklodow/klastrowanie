#!/usr/bin/env python3

# Takes a fasta file with headers like:
# >2k7w B 5wos B 4qvf B
# and writes a file with headers like:
# >2k7wB 5wosB 4qvfB

import sys

for line in open(sys.argv[1]):
    if line[0] == '>':
        tokens = line.strip().split()
        sys.stdout.write(tokens[0])
        for t in tokens:
            if len(t) > 1:
                sys.stdout.write(" ")
            sys.stdout.write(t)
        sys.stdout.write("\n")
    else:
        print(line,end="")

