#!/usr/bin/env python3
import sys

info = """
Reads a file produced by pairwise_identity_for_clustering application; prints distances for clustering

Assumed format of the input file:

#i_seq  j_seq :   seq_id n_identical len_ali_no_tails len_ali_full i_len j_len
4z9vF 5o9kA :   0.9500        19    19   141     20   141

USAGE:
    python3 similarity_to_distance.py seq_id_ouput
"""

if len(sys.argv) == 1:
    print(info)
    sys.exit(0)

for line in open(sys.argv[1]):
  if line[0] == "#" : continue
  tokens = line.strip().split()
  if len(tokens) != 9: continue
  shorter_seq_len = min(int(tokens[7]),int(tokens[8]))
  # real value of sequence identity computed for a semi-global alignment
  d = float(tokens[4]) / max(float(tokens[5]),shorter_seq_len)
  # identity to a byte distace
  if d < 0.36: continue
  d = (1.0 - d)*400
  if d >=256: continue
  print(tokens[0],tokens[1],int(d))
